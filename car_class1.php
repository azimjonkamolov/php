<?php

class Car
{

    function moveWheels()
    {
        echo "Wheels are moving";
    }

}

if(method_exists("Car", "moveWheels")) 
{
    echo "Amigo is moving";
}
else
{
    echo "Amigo ain't no moving";
}

?>