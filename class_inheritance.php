<?php

class Car
{

    var $doors = 4;
    var $engine = 1;

    function moveWheels()
    {
        echo "Wheels are moving";
    }
}

class Plane extends Car
{

}

$jet = new Plane();

echo $jet -> doors;   

?>

