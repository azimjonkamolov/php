<?php

    $name = "someName";
    $val = 100;
    $expiration = time() + (60 * 60 * 24 * 365);

    setcookie($name, $val, $expiration);

    echo "You got a cookie";
    echo "<br>";
?>

<?php

if(isset($_COOKIE["someName"]))
{
    $someOne = $_COOKIE["someName"];

    echo $someOne;
}
else
{
    echo "";
}

?>