<?php

class Car
{
    function moveWheels()
    {
        echo "Wheels are moving";
    }
}

$bmw = new Car();
$tesla = new Car();

$bmw->moveWheels();

?>

