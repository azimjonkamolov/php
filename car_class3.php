<?php

class Car
{
    var $wheels = 4;
    var $engine = 1;
    var $doors = 4;

    function moveWheels()
    {
        echo "Wheels are moving";
    }

    function doorNumber()
    {
        $this -> doors = 8;
    }
}

$bmw = new Car();
$tesla = new Car();

echo $tesla->doors . "</br>";

$bmw->moveWheels();

echo "</br>";

$bmw->doorNumber();
echo $bmw->doors;


?>

